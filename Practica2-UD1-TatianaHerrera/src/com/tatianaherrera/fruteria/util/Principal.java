package com.tatianaherrera.fruteria.util;

import com.tatianaherrera.fruteria.gui.FruteriaControlador;
import com.tatianaherrera.fruteria.gui.FruteriaModelo;
import com.tatianaherrera.fruteria.gui.Ventana;

/**
 * @author tatianaherrera
 * Es la clase principal del programa
 *
 */
public class Principal {


    public static void main(String[] args) {
        Ventana vista= new Ventana();
        FruteriaModelo modelo= new FruteriaModelo();
        FruteriaControlador controlador= new FruteriaControlador(vista,modelo);
    }
}
