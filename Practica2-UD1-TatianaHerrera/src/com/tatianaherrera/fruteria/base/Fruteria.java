package com.tatianaherrera.fruteria.base;

import java.time.LocalDate;

public abstract class Fruteria {
    private String nombre;
    private String tipo;
    private String origen;
    private String marca;
    private String clasificacion;
    private LocalDate fechaCaducidad;

    public Fruteria(String nombre, String tipo, String origen, String marca, String clasificacion, LocalDate fechaCaducidad){
        this.nombre=nombre;
        this.tipo=tipo;
        this.origen=origen;
        this.marca=marca;
        this.clasificacion=clasificacion;
        this.fechaCaducidad=fechaCaducidad;
    }

    public Fruteria() {

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca=marca;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public LocalDate getFechaCaducidad() {
        return (fechaCaducidad);
    }

    public void setFechaCaducidad(LocalDate fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }
}


