package com.tatianaherrera.fruteria.base;

import java.time.LocalDate;

/**
 * @author tatianaherrera
 */

public class Fruta extends Fruteria {
    private int kilo;

    public Fruta(){
        super();
    }


    public Fruta(String nombre, String tipo, String origen, String marca,String clasificacion, LocalDate fechaCaducidad,int kilo) {
        super(nombre, tipo, origen, marca,clasificacion, fechaCaducidad);
        this.kilo=kilo;

    }



    public int getKilo() {
        return kilo;
    }

    public void setKilo(int kilo) {
        this.kilo = kilo;
    }

    @Override
    public String toString(){
        return "Fruta:" +getNombre()+""+getTipo()+""
                +getOrigen()+""+getMarca()+""+getKilo();
    }
}
