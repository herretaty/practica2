package com.tatianaherrera.fruteria.base;

import java.time.LocalDate;

/**
 * @author tatianaherrera
 */

public class Verdura extends Fruteria{
    private double gramos;

    public Verdura(){
        super();
    }

    public Verdura(String nombre, String tipo, String origen, String marca, String clasificacion, LocalDate fechaCaducidad, double gramos){
        super(nombre, tipo, origen, marca,clasificacion, fechaCaducidad);
        this.gramos=gramos;
    }



    public double getGramos() {
        return gramos;
    }

    public void setGramos(double gramos) {
        this.gramos = gramos;
    }

    @Override
    public String toString(){
        return "Verdura:" +getNombre()+""+getTipo()+""
                +getOrigen()+""+getMarca()+""+getGramos();
    }
}
