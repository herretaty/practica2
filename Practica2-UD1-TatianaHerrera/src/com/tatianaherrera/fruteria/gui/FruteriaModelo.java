package com.tatianaherrera.fruteria.gui;

import com.tatianaherrera.fruteria.base.Fruta;
import com.tatianaherrera.fruteria.base.Fruteria;
import com.tatianaherrera.fruteria.base.Verdura;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;


/**
 * @author tatianaherrera
 */
public class FruteriaModelo {
    private ArrayList<Fruteria> listaFruteria;


    public FruteriaModelo() {
        listaFruteria = new ArrayList<Fruteria>();
    }

    /**
     * @return
     */
    public ArrayList<Fruteria> obtenerFruteria() {
        return listaFruteria;
    }

    /**
     * Este metedo es el encargado de almacenar todos nuestros atributos que le hemos puesto
     * y añadirlos en la arrayList
     * @param nombre
     * @param tipo
     * @param origen
     * @param marca
     * @param fechaCaducidad
     * @param kilo
     */
    public void altaFruta(String nombre, String tipo, String origen, String marca,String clasificacion, LocalDate fechaCaducidad, int kilo) {
        Fruta nuevaFruta=new Fruta(nombre,tipo, origen,marca,clasificacion,fechaCaducidad,kilo);
        listaFruteria.add(nuevaFruta);
    }

    public void altaVerdura(String nombre, String tipo, String origen, String marca,String clasificacion, LocalDate fechaCaducidad, double gramos) {
        Verdura nuevaVerdura= new Verdura(nombre,tipo,origen,marca, clasificacion, fechaCaducidad,gramos);
        listaFruteria.add(nuevaVerdura);
    }


    public boolean existeNombre (String nombre){
        for (Fruteria unFruteria : listaFruteria) {
            if (unFruteria.getNombre().equals(nombre)) {
                return true;
                }
            }
            return false;
        }


    /**
     * En este metodo nos exportara un fichero xml que nosotros guardaremos en nuestro equipo
     * @param fichero
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
        public void exportarXML (File fichero) throws ParserConfigurationException, TransformerException {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();
            Document documento = dom.createDocument(null, "xml", null);


            Element raiz = documento.createElement("Vehiculos");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoFruteria = null, nodoDatos = null;
            Text texto = null;

            for (Fruteria unFruteria : listaFruteria) {

                if (unFruteria instanceof Fruta) {
                    nodoFruteria = documento.createElement("Fruta");

                } else {
                    nodoFruteria = documento.createElement("Verdura");
                }
                raiz.appendChild(nodoFruteria);


                nodoDatos = documento.createElement("nombre");
                nodoFruteria.appendChild(nodoDatos);

                texto = documento.createTextNode(unFruteria.getNombre());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("tipo");
                nodoFruteria.appendChild(nodoDatos);

                texto = documento.createTextNode(unFruteria.getTipo());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("origen");
                nodoFruteria.appendChild(nodoDatos);

                texto = documento.createTextNode(unFruteria.getOrigen());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("precio");
                nodoFruteria.appendChild(nodoDatos);

                texto = documento.createTextNode(unFruteria.getMarca());
                nodoDatos.appendChild(texto);

                nodoDatos=documento.createElement("clasificacion");
                nodoFruteria.appendChild(nodoDatos);

                texto= documento.createTextNode(unFruteria.getClasificacion());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("fecha-caducidad");
                nodoFruteria.appendChild(nodoDatos);

                texto = documento.createTextNode(unFruteria.getFechaCaducidad().toString());
                nodoDatos.appendChild(texto);


                if (unFruteria instanceof Fruta) {
                    nodoDatos = documento.createElement("kilo");
                    nodoFruteria.appendChild(nodoDatos);
                    texto = documento.createTextNode(String.valueOf(((Fruta) unFruteria).getKilo()));

                } else {
                    nodoDatos = documento.createElement("gramos");
                    nodoFruteria.appendChild(nodoDatos);
                    texto = documento.createTextNode(String.valueOf(((Verdura) unFruteria).getGramos()));
                }
                nodoDatos.appendChild(texto);


            }
            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(fichero);

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, resultado);
        }

    /**
     * En este metodo, nos permitira importar, el archivo exportado desde nuestra aplicacion,
     * enseñandonos los datos que hemos añadido
     * @param fichero
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
        public void importarXML (File fichero) throws ParserConfigurationException, IOException, SAXException {
            listaFruteria = new ArrayList<Fruteria>();
            Fruta nuevaFruta = null;
            Verdura nuevaVerdura = null;

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(fichero);

            NodeList listaElementos = documento.getElementsByTagName("*");

            for (int i = 0; i < listaElementos.getLength(); i++) {
                Element nodoFruteria = (Element) listaElementos.item(i);


                if (nodoFruteria.getTagName().equals("Fruta")) {
                    nuevaFruta = new Fruta();
                    nuevaFruta.setNombre(nodoFruteria.getChildNodes().item(0).getTextContent());
                    nuevaFruta.setTipo(nodoFruteria.getChildNodes().item(1).getTextContent());
                    nuevaFruta.setOrigen(nodoFruteria.getChildNodes().item(2).getTextContent());
                    nuevaFruta.setMarca(nodoFruteria.getChildNodes().item(2).getTextContent());
                    nuevaFruta.setClasificacion(nodoFruteria.getChildNodes().item(4).getTextContent());
                    nuevaFruta.setFechaCaducidad(LocalDate.parse(nodoFruteria.getChildNodes().item(5).getTextContent()));
                    nuevaFruta.setKilo(Integer.parseInt(nodoFruteria.getChildNodes().item(6).getTextContent()));

                    listaFruteria.add(nuevaFruta);
                } else if
                    (nodoFruteria.getTagName().equals("Verdura")) {
                    nuevaVerdura = new Verdura();
                    nuevaVerdura.setNombre(nodoFruteria.getChildNodes().item(0).getTextContent());
                    nuevaVerdura.setTipo(nodoFruteria.getChildNodes().item(1).getTextContent());
                    nuevaVerdura.setOrigen(nodoFruteria.getChildNodes().item(2).getTextContent());
                    nuevaVerdura.setMarca(nodoFruteria.getChildNodes().item(3).getTextContent());
                    nuevaVerdura.setNombre(nodoFruteria.getChildNodes().item(4).getTextContent());
                    nuevaVerdura.setFechaCaducidad(LocalDate.parse(nodoFruteria.getChildNodes().item(5).getTextContent()));
                    nuevaVerdura.setGramos(Double.parseDouble(nodoFruteria.getChildNodes().item(6).getTextContent()));
                    listaFruteria.add(nuevaVerdura);


                }
            }
        }
    }







