package com.tatianaherrera.fruteria.gui;

import com.tatianaherrera.fruteria.base.Fruteria;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

/**
 * @author tatianaherrera
 */
public class Ventana {
    private JPanel panel1;
    public JFrame frame;
    public JRadioButton frutaRadioButton;
    public JLabel Nombre;
    public JTextField nombretxt;
    public JLabel Tipo;
    public JTextField tipotxt;
    public JLabel Origen;
    public JTextField origentxt;
    public JLabel Marca;
    public JTextField marcatxt;
    public JLabel Clasificacion;
    public JTextField clasificaciontxt;
    public JLabel fechaCaducidad;
    public JTextField kilostxt;
    public JButton nuevoBtn;
    public JButton exportarBtn;
    public JButton importarBtn;
    public JList list1;
    public JRadioButton verduraRadioButton;
    public JLabel fruteriaLabel;
    public JLabel kilosGramosLbl;
    public DatePicker fechaCaducidadDPicker;


    public DefaultListModel<Fruteria> dlmFruteria;

    public Ventana(){
        frame= new JFrame("Fruteria");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    private void initComponents() {
        dlmFruteria= new DefaultListModel<Fruteria>();
        list1.setModel(dlmFruteria);
    }
}