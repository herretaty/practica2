package com.tatianaherrera.fruteria.gui;

import com.tatianaherrera.fruteria.base.Fruta;
import com.tatianaherrera.fruteria.base.Fruteria;
import com.tatianaherrera.fruteria.base.Verdura;
import com.tatianaherrera.fruteria.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;

import static java.lang.Double.*;

/**
 * @author tatianaherrera
 * Esta clase fruteriaControlar es el encargado de procesar los mandatos y crear eventos que el usuario necesita
 * como botones o componentes, definiendo los metedos que complementan la clase
 */
public class FruteriaControlador implements ActionListener, ListSelectionListener, WindowListener {
    private Ventana vista;
    private FruteriaModelo modelo;
    private File ultimaRutaExportada;

    /**
     * En este metodo nos permite acceder a la clase vista y fruteriaModelo,
     * tambien hace uso de un try-cash para que no nos falle las instrucciones
     * @param vista
     * @param modelo
     */
    public FruteriaControlador(Ventana vista, FruteriaModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);

    }

    /**
     * En este metodo se generan las acciones que se producen al al pulsar los botones que tenemos,
     * los textos y las cajas de texto
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch (actionCommand) {
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios\n" +
                            "Nombre\nTipo\nOrigen\nPrecio\nMarca\n"+
                            vista.kilosGramosLbl.getText());
                    break;
                }

                if (modelo.existeNombre(vista.nombretxt.getText())) {
                    Util.mensajeError("Ya existe una fruta con ese nombre\n+" +
                            vista.nombretxt.getText());
                    break;
                }
                if (vista.frutaRadioButton.isSelected()) {
                    modelo.altaFruta(vista.nombretxt.getText(),vista.tipotxt.getText(), vista.origentxt.getText(),
                            vista.marcatxt.getText(), vista.clasificaciontxt.getText(),vista.fechaCaducidadDPicker.getDate(),
                            Integer.parseInt(vista.kilostxt.getText()));
                } else {
                    modelo.altaVerdura(vista.nombretxt.getText(),vista.tipotxt.getText(), vista.origentxt.getText(),
                            vista.marcatxt.getText(), vista.clasificaciontxt.getText(),vista.fechaCaducidadDPicker.getDate(),
                            parseDouble(vista.kilostxt.getText()));
                }
                limpiarCampos();
                refrescar();
                break;
        case "Importar":
        JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivo XML", "xml");
        int opt = selectorFichero.showOpenDialog(null);
        if (opt == JFileChooser.APPROVE_OPTION) {
            try {
                modelo.importarXML(selectorFichero.getSelectedFile());
            } catch (ParserConfigurationException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (SAXException ex) {
                ex.printStackTrace();
            }
            refrescar();
        }
        break;
        case "Exportar":
        JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML", "xml");
        int opt2 = selectorFichero2.showSaveDialog(null);
        if (opt2 == JFileChooser.APPROVE_OPTION) {
            try {
                modelo.exportarXML(selectorFichero2.getSelectedFile());
                actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
            } catch (ParserConfigurationException ex) {
                ex.printStackTrace();
            } catch (TransformerException ex) {
                ex.printStackTrace();
            }
        }
        break;
        case "Fruta":
        vista.kilostxt.setText("kilo");
        break;
        case "Verdura":
        vista.kilostxt.setText("gramos");
        break;


            default:
                throw new IllegalStateException("Unexpected value: " + actionCommand);
        }
}
     /**
     * En este metodo booleano lo que hace es devolvernos un valor del tipo de datos
     * que tenemos almacenados
     * @return
     */
    private boolean hayCamposVacios() {
        if (vista.kilostxt.getText().isEmpty() ||
                vista.nombretxt.getText().isEmpty() ||
                vista.tipotxt.getText().isEmpty() ||
                vista.origentxt.getText().isEmpty() ||
                vista.marcatxt.getText().isEmpty()||
                vista.clasificaciontxt.getText().isEmpty()||
                vista.fechaCaducidadDPicker.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    private void limpiarCampos() {
        vista.kilostxt.setText(null);
        vista.nombretxt.setText(null);
        vista.tipotxt.setText(null);
        vista.origentxt.setText(null);
        vista.marcatxt.setText(null);
        vista.clasificaciontxt.setText(null);
        vista.fechaCaducidadDPicker.setText(null);
        vista.nombretxt.requestFocus();
    }


    private void refrescar() {
        vista.dlmFruteria.clear();
        for (Fruteria unVehiculo : modelo.obtenerFruteria()) {
            vista.dlmFruteria.addElement(unVehiculo);
        }
    }

    /**
     * Este metodo permite que los botones que hemos creado hagan su funcion al presionarlos
     * @param listener
     */
    private void addActionListener(ActionListener listener) {
        vista.frutaRadioButton.addActionListener(listener);
        vista.verduraRadioButton.addActionListener(listener);
        vista.exportarBtn.addActionListener(listener);
        vista.importarBtn.addActionListener(listener);
        vista.nuevoBtn.addActionListener(listener);
    }

    /**
     *Este metodo se encarga de ejecutar los eventos que requiere nuestra ventana
     * @param listener
     */
    private void addWindowListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    /**
     * Este metodo es el encargado de cargar los datos en fruteria.conf
     * @throws IOException
     */
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("fruteria.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    /**
     * Este metodo nos actualizada la informacion, cada vez que iniciamos la aplicacion
     * @param ultimaRutaExportada
     */
    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    /**
     * Este metodo es el encargado de guardar la ultima informacion procedente de la ejecucion
     * del programa
     * @throws IOException
     */
    private void guardarConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("fruteria.conf"), "Datos configuracion de la fruteria");

    }

    /**
     * Este metodo hace la funcion de que podamos cambiar de componente al seleccionar lo que queremos
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            Fruteria fruteriaSeleccionada = (Fruteria) vista.list1.getSelectedValue();
            vista.nombretxt.setText((fruteriaSeleccionada.getNombre()));
            vista.tipotxt.setText(fruteriaSeleccionada.getTipo());
            vista.origentxt.setText(fruteriaSeleccionada.getOrigen());
            vista.marcatxt.setText(fruteriaSeleccionada.getMarca());
            vista.clasificaciontxt.setText(fruteriaSeleccionada.getClasificacion());
            vista.fechaCaducidadDPicker.setDate(fruteriaSeleccionada.getFechaCaducidad());

            if (fruteriaSeleccionada instanceof Fruta) {
                vista.frutaRadioButton.doClick();
                vista.kilostxt.setText(String.valueOf(((Fruta) fruteriaSeleccionada).getKilo()));
            } else {
                vista.verduraRadioButton.doClick();
                vista.kilostxt.setText(String.valueOf(((Verdura) fruteriaSeleccionada).getGramos()));
            }
        }
    }

    /**
     * Este metodo es el que nos adapta la ventana que nos recuerda si queremos cerrarla
     * enseñando un mensaje
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.mensajeConfirmacion("¿Desea salir de la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }

    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }
}
